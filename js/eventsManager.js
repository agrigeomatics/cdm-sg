function handleEvents() {

    $("#tabByMonth").click(function(evt) {
        byId('TSpaneTable').appendChild(byId('TSdivTableResults'));
        byId('TSpaneGraph').appendChild(byId('TSdivGraphResults'));

        byId('paneTable').appendChild(byId('MOdivTableResults'));
        byId('paneGraph').appendChild(byId('MOdivGraphResults'));

        let layer = CDM_SG.map.getLayer("CDMRaster");
        layer.setDefinitionExpression(CDM_SG.MOcurrentCDMlayer);
    });
    $("#tabTimeSeries").click(function(evt) {
        byId('MOpaneTable').appendChild(byId('MOdivTableResults'));
        byId('MOpaneGraph').appendChild(byId('MOdivGraphResults'));

        byId('paneTable').appendChild(byId('TSdivTableResults'));
        byId('paneGraph').appendChild(byId('TSdivGraphResults'));

        let layer = CDM_SG.map.getLayer("CDMRaster");
        layer.setDefinitionExpression(CDM_SG.TScurrentCDMlayer);
    });

    $("#lboYear").on ("change", function(evt) {
        updateMonths("#lboMonth", this.value, false);
    });
    $("#lboYear").on ("change", function(evt) {
        updateMonths("#lboMonth", this.value, false);
    });
    $("#lboStartYear").on ("change", function(evt) {
        updateMonths("#lboStartMonth", this.value, false);
    });
    $("#lboEndYear").on ("change", function(evt) {
        updateMonths("#lboEndMonth", this.value, false);
    });
        
    $("#btnGOTS").click(function(evt) {
        require(["CDM_SG/js/histoGraph"], function(histoGraph) {
            histoGraph.execQuery();
        });
    });

    $("#btnGO").click(function(evt) {
        var rasterName;
        require(["CDM_SG/js/mapManager"], function(mapManager) {
            var region = $('#lboRegion').find(":selected").val();
            mapManager.zoomTo(region);
            rasterName = mapManager.showCDMLayer();
        });
        require(["CDM_SG/js/chartsManager"], function(chartsManager) {
            chartsManager.execQuery();
        });
        $("#panelGraph").on("chartDone2", function(event) {
            $(this).unbind("chartDone2");
            console.log("chart/done2");
            require(["CDM_SG/js/archiveTable"], function(archiveTable) {
                archiveTable.prepArchiveTable(rasterName);
            });
        });
    });

    $(".btnExpandReduce").click(function(evt) {
        var src = evt.target.getAttribute("src");
        var ind = src.substr(src.indexOf('/')+1,6);
        var newInd = ind == "reduce" ? "expand" : "reduce";
        fExpandReduce(ind, evt.target.parentElement.getAttribute("data-in"));
        evt.target.setAttribute("src", src.replace(ind, newInd))
    });
    
}

function startBusy() {
    var docBody = document.body;
    var style = "left: " + (docBody.scrollWidth / 2 - 125) + "px;" +
                " top: " + (docBody.scrollHeight / 2 - 25) + "px;";
    var style = "left: " + (screen.width / 2 - 25) + "px;" +
                " top: " + (screen.height / 2 - 25) + "px;";                          
    $('body').append('<div id="div_busy" style="' + style + '"><div id="img_busy"></div></div>');

    byId("table-title-units").innerHTML = "";
    $("#btnGO").prop("disabled", true);
    $("#btnGO").css("color", "lightgrey");
    $("#btnTSGO").prop("disabled", true);
    $("#btnTSGO").css("color", "lightgrey");
}

function stopBusy() {
    var elem = document.getElementById("div_busy");
    if (elem)
        elem.parentNode.removeChild(elem);

    $("#btnGO").prop("disabled", false);
    $("#btnGO").css("color", "black");
    $("#btnTSGO").prop("disabled", false);            
    $("#btnTSGO").css("color", "black");
}

function fExpandReduce(ind, panel) {
    let sPanelBody = panel.replace("panel", "panelBody");
    if (ind == "reduce") {
        byId(sPanelBody).style.display = 'none';
    }
    if (ind == "expand") {
        byId(sPanelBody).style.display = 'block';
    }
};

function updateMonths(lboMonths, sYear, bFlag) {
    let sel = lboMonths == "#lboStartMonth" ? "selected": "";
    if (!bFlag) sel = "selected";
    $(lboMonths).children().remove().end();
    $(lboMonths).append('<option value="01" ' + sel + '>January</option>');
    
    $(lboMonths).append('<option value="02">February</option>');
    $(lboMonths).append('<option value="03">March</option>');
    $(lboMonths).append('<option value="04">April</option>');
    $(lboMonths).append('<option value="05">May</option>');
    $(lboMonths).append('<option value="06">June</option>');
    $(lboMonths).append('<option value="07">July</option>');
    $(lboMonths).append('<option value="08">August</option>');

    sel = (lboMonths == "#lboMonth" || lboMonths == "#lboEndMonth") && bFlag ? "selected" : "";
    $(lboMonths).append('<option value="09" ' + sel + '>September</option>');

    if (sYear != "2017") {
        $(lboMonths).append('<option value="10">October</option>');
        $(lboMonths).append('<option value="11">November</option>');
        $(lboMonths).append('<option value="12">December</option>');
    }
}



function initCriterias() {
    function initYears(lboYears) {
        for (var i = 2017; i >= 2002; i--) {
            let sel = "";
            if ((lboYears == "#lboYear" || lboYears == "#lboEndYear") && i == 2017) sel = "selected";
            if (lboYears == "#lboStartYear" && i == 2016) sel = "selected";  // Laurier; problem with 2002, so start at 2003
            $(lboYears).append('<option ' + sel + ' value="' + i + '">' + i + '</option>');
        }
    }

    initYears("#lboYear");
    updateMonths("#lboMonth", "2017", true);
    initYears("#lboStartYear");
    updateMonths("#lboStartMonth", "2003", true);
    initYears("#lboEndYear");
    updateMonths("#lboEndMonth", "2017", true);

    handleEvents();
}
