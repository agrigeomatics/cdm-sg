define([
        "esri/map",
        "esri/layers/ArcGISImageServiceLayer",
        "esri/tasks/query",
        "esri/tasks/QueryTask",
        "esri/symbols/SimpleFillSymbol",
        "esri/graphic",
        "esri/graphicsUtils",
        "esri/dijit/HomeButton"
       ],
       function(Map,
                ArcGISImageServiceLayer,
                Query,
                QueryTask,
                SimpleFillSymbol,
                Graphic,
                GraphicsUtils,
                HomeButton
               )

    {
        return {

            initMap: function() {
                CDM_SG.map = new Map("divMap", {
                    basemap: "topo",
                    center: [-95, 57],
                    zoom: 4,
                    minZoom: 4
                });
                var home = new HomeButton({
                    map: CDM_SG.map
                }, "HomeButton");
                home.startup();                
        
                var imageServiceLayer = new ArcGISImageServiceLayer(CDM_SG.urls.CDM, {id: "CDMRaster"});
                imageServiceLayer.setDefinitionExpression("name = 'none'");
                imageServiceLayer.setOpacity(0.4);
                CDM_SG.MOcurrentCDMlayer = "name = 'none'";
        
                CDM_SG.map.addLayer(imageServiceLayer);

                var query = new Query();
                query.returnGeometry = false;
                query.where = "1 = 1";
                query.outFields = ["*"];
                var queryTask = new QueryTask(CDM_SG.urls.CCS_boundaries + '/0'); // CCS_WEBM 
                queryTask.execute(query, function(results) {
                    for (var i = 0; i < results.features.length; i++) {
                        var attrs = results.features[i].attributes;
                        CDM_SG.aCCSuid.push({OBJECTID: attrs.OBJECTID,
                                             CCSUID: attrs.CCSUID,
                                             CCSOIDlong: attrs.CCSOIDlong  })
                    }
                });

                var query = new Query();
                query.returnGeometry = false;
                query.where = "1 = 1";
                query.outFields = ["OBJECTID", "FID_fishnet25", "FID_CCS_WEBM"];
                var queryTask = new QueryTask(CDM_SG.urls.CCS_boundaries + '/1'); // CCS_fishnet25 
                queryTask.execute(query, function(results) {
                    for (var i = 0; i < results.features.length; i++) {
                        var attrs = results.features[i].attributes;
                        var sFID_fishnet25 = attrs.FID_fishnet25.toString();
                        if (!CDM_SG.CCS_fishnet25[sFID_fishnet25])
                            CDM_SG.CCS_fishnet25[sFID_fishnet25] = [];
                        CDM_SG.CCS_fishnet25[sFID_fishnet25].push(attrs.FID_CCS_WEBM);
                    }
                });

                /*var query = new Query();
                query.returnGeometry = false;
                query.where = "1 = 1";
                query.outFields = ["OID"];
                var queryTask = new QueryTask(CDM_SG.urls.CCS_boundaries + '/2'); // fishnet25 
                queryTask.execute(query, function(results) {
                    for (var i = 0; i < results.features.length; i++) {
                        var attrs = results.features[i].attributes;
                        CDM_SG.aFishnet25.push({OBJECTID: attrs.OID })
                    }
                });*/
            },

            zoomTo: function (region) {
                var query = new Query();
                query.returnGeometry = true;
                query.where = "REGION_ID = " + region;
                var queryTask = new QueryTask(CDM_SG.urls.regions);
                queryTask.execute(query, zoomTo2);                    
            },
            
            showFirstCDMLayer: function () {
                CDM_SG.map.on("load", function () {
                    if (CDM_SG.bFirstMap) {
                        var rasterName; 

                        require(["CDM_SG/js/mapManager"], function(mapManager) {
                            rasterName = mapManager.showCDMLayer();
                        });
                        require(["CDM_SG/js/chartsManager"], function(chartsManager) {
                            chartsManager.execQuery();
                        });
                        $("#panelGraph").on("chartDone1", function(event) {
                            $(this).unbind("chartDone1");                            
                            console.log("chart/done1");
                            require(["CDM_SG/js/archiveTable"], function(archiveTable) {
                                archiveTable.prepArchiveTable(rasterName);
                            });
                        });
                    }
                })
            },

            showCDMLayer: function (rasterName) {
                if (!rasterName)
                    rasterName = getRasterName();

                var layer = CDM_SG.map.getLayer("CDMRaster");
                layer.setDefinitionExpression("name = '" + rasterName + "'");
                if ($( "#tabsMain").tabs("option", "active") == 0)
                    CDM_SG.MOcurrentCDMlayer = "name = '" + rasterName + "'";
                else
                    CDM_SG.TScurrentCDMlayer = "name = '" + rasterName + "'";

                var rasterName2 = rasterName.replace(/CDM_/i, '');
                rasterName2 = rasterName2.replace(/_/g, '-');
                byId("divMapTitle").innerHTML = rasterName2;
                
                return rasterName;
            },

            clearCDMLayer: function () {
                var layer = CDM_SG.map.getLayer("CDMRaster");
                layer.setDefinitionExpression("name = 'none'");
                if ($( "#tabsMain").tabs("option", "active") == 0)
                    CDM_SG.MOcurrentCDMlayer = "name = 'none'";
                else
                    CDM_SG.TScurrentCDMlayer = "name = 'none'";
                
                //byId("titleMap").innerHTML = "Map";
            },

            removeGraphic: function (graphicId) {
                let graphics = CDM_SG.map.graphics.graphics;
                for (let i = 0; i < graphics.length; i++) {
                    if (graphics[i].id == graphicId)
                        CM2.map.graphics.remove(graphics[i]);
                }
            },

             findGraphic: function(graphicId) {
                let graphics = CDM_SG.map.graphics.graphics;
                for (let i = 0; i < graphics.length; i++) {
                    if (graphics[i].id == graphicId)
                        return graphics[i];
                }
            },

            addGraphic: function(geometry, id) {
                this.removeGraphic(id);
                /*let json = JSON.parse(`
                {
                    "hiliteSymbol": {
                        "type": "esriSFS",
                        "style": "esriSFSSolid",
                        "color": [215,76,0,64],
                        "outline": {
                            "type": "esriSLS",
                            "style": "esriSLSSolid",
                            "color": [110,110,110,255],
                            "width": 1
                        }
                    }            
                }
                `);*/
                let sfs = new SimpleFillSymbol(CDM_SG.hiliteSymbol);            
                let graphic = new Graphic(geometry, sfs);
                graphic.id = id;
                CDM_SG.map.graphics.add(graphic);
                return graphic;
            }

        }

        function zoomTo2(results) {
            var extent = GraphicsUtils.graphicsExtent(results.features);
            CDM_SG.map.setExtent(extent.expand(1.33));
        }

    }
);
