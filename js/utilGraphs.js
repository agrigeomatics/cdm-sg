define(["dojo/promise/all",  
        "dojo/request/xhr",
        "CDM_SG/js/mapManager"
       ],
       function(all, xhr, mapManager)
    {

        return {

            computeHistogram: function(params, results) {
                var proxyUrl = "/DotNet/proxy.ashx";
                url = proxyUrl + "?" + CDM_SG.urls.CDM + "/computeStatisticsHistograms";

                var geometry = results.features[0].geometry;
                var regionName = results.features[0].attributes.region;
                if (getGraphId().indexOf("Ext") == -1)
                    CDM_SG.graphInfo.totalArea = results.features[0].attributes.Shape_Area;
                else if (params.type == "map1Month")
                    CDM_SG.graphInfo.totalData = results.features[0].attributes.area;
                var histo_data = {
                    geometryType: "esriGeometryPolygon",
                    geometry: JSON.stringify(geometry.toJson()),
                    f: "pjson"                    
                };
    
                if (params.type == "histoGraph") {
                    var sYear = $('#lboStartYear').find(":selected").val();
                    var sMonth = $('#lboStartMonth').find(":selected").val();
                } else {
                    var sYear = $('#lboYear').find(":selected").val();
                    var sMonth = $('#lboMonth').find(":selected").val();
                }
                if (Number(sMonth) < 12) {
                    var sMonth = (Number(sMonth) + 1).toString();
                    sMonth = sMonth.length == 1 ? '0' + sMonth : sMonth;
                    var sDate = sYear + '-' + sMonth + "-01T12:00:00";
                } else {
                    var sDate = (Number(sYear) + 1) + "-01-01T12:00:00";
                }
                var startDate = new Date(sDate); // "2002-12-01T12:00:00"

                if (params.type == "histoGraph") {
                    sYear = byId("lboEndYear").options[byId("lboEndYear").selectedIndex].value;
                    sMonth = byId("lboEndMonth").options[byId("lboEndMonth").selectedIndex].value.substr(0,2);
                    sMonth = (Number(sMonth) + 1).toString();
                    sMonth = sMonth.length == 1 ? '0' + sMonth : sMonth;
                    var sDate = sYear + '-' + sMonth + "-01T12:00:00";
                    var endDate = new Date(sDate); // "2017-10-01T12:00:00");
                    mapManager.clearCDMLayer();
                } else {
                    var rasterName = mapManager.showCDMLayer();
                    var endDate = startDate;
                }
        
                var aPromises = new Array();
                var aNames = new Array();
        
                var dte1 = new Date(startDate.getTime());;
                while (dte1 <= endDate) {
                    var dte2 =  new Date(dte1.getTime());
                    var dte2 =  new Date(dte2.setDate(dte2.getDate()-1));
                    var sdte1 = dte2.toISOString().substr(0,10);
                    var sdte2 = sdte1.replace(/-/g, "_");
                    var sname = "cdm_" + sdte2;

                    histo_data.mosaicRule =  JSON.stringify({
                        mosaicMethod: "esriMosaicNone",
                        where: "Name = '" + sname + "'"
                    });

                    var xhrRes = xhr(url, {
                        method: "POST",
                        data: histo_data,
                        timeout: 60000,
                        handleAs: "json"
                    });
                    aPromises.push(xhrRes);
                    aNames.push(sname);

                    dte1 = new Date(dte1.setMonth(dte1.getMonth()+1));
                };

                if (params.type == "map1Month") {
                    for (var i=1; i <= 4; i++) {
                        var strtDte = new Date(startDate.getTime());
                        var dte = new Date(startDate.getTime());
                        switch(i) {
                            case 1:   // 1 months ago
                                dte = new Date(dte.setMonth(strtDte.getMonth()-1 ));
                                dte = new Date(dte.setDate(dte.getDate()-1 ));
                                break;
                            case 2:   // 3 months ago
                                dte = new Date(dte.setMonth(strtDte.getMonth()-3 ));
                                dte = new Date(dte.setDate(dte.getDate()-1 ));
                                break;
                            case 3:   // begin of growing season
                                var sDate = strtDte.getFullYear() + "-05-01T12:00:00";
                                dte = new Date(sDate);
                                dte = new Date(dte.setDate(dte.getDate()-1 ));
                                break;
                            case 4:  // 1 year ago
                                dte = new Date(dte.setYear(strtDte.getFullYear()-1 ));
                                dte = new Date(dte.setDate(dte.getDate()-1 ));
                                break;
                        }
                        var sdte1 = dte.toISOString().substr(0,10);
                        var sdte2 = sdte1.replace(/-/g, "_");
                        var sname = "cdm_" + sdte2;
        
                        histo_data.mosaicRule =  JSON.stringify({
                            mosaicMethod: "esriMosaicNone",
                            where: "Name = '" + sname + "'"
                        });
        
                        var xhrRes = xhr(url, {
                            method: "POST",
                            data: histo_data,
                            timeout: 60000,
                            handleAs: "json"
                        });
                        aPromises.push(xhrRes);
                        aNames.push(sname);
                    }
                };
                console.log("aNames: ", aNames); // utile

                if (params.type != "histoGraph") {
                    CDM_SG.graphInfo.CDMLayerName = sname;
                }
                    
                var aPeriods = [];
                $("body").css("cursor", "wait");
                console.log("computeHistogram before promiseAll");
                startBusy();
                all(aPromises).then(function (aResults) {
                    console.log("computeHistogram after promiseAll");
                    $("body").css("cursor", "default");
                    for (var i = 0; i < aResults.length; i++) {
                        if (aResults[i].histograms) {
                            var aRow = {};
                            //console.log('\n' + aNames[i] + '\n' + JSON.stringify(aResults[i]));
                            var results =  aResults[i].histograms[0].counts;
                            var period = aNames[i].substr(4);
                            period = period.replace(/_/g, '-');
                            aRow.date = period;
                            var cum_count = 0;
                            var cum_area = 0;
                            var row0 = null;
                            //if (results[5+128] > 0) debugger; // 5: drought not analyzed
                            for (var j = 4; j >= -1; j--) { // so ignored 5: drought not analyzed
                                var value = results[j+128];
                                cum_count += value;
                                var area = value; // pixel is 5 km by 5 km
                                cum_area += area;
                                if (j != -1) {
                                    aRow['D'+j] = value;
                                } else {
                                    aRow.noData = value;
                                }
                            }
                            //aRow.totData = cum_count;
                            //aRow.totArea = cum_area;
                            aPeriods.push(aRow);
                        } else {
                            console.log("Error for ", aNames[i])
                        }
                    }
                    console.log("aPeriods: ", JSON.stringify(aPeriods));  // utile

                    var tableTitle = "tableTitle";
                    if (params.type == "histoGraph") {
                        /*var lboFormatPeriod = byId("lboFormatPeriod");
                        if (lboFormatPeriod.options[lboFormatPeriod.selectedIndex].value == "StackedAreaChart") 
                            D3HistoGraph(aPeriods);
                        else {
                            HistoTable(aPeriods, tableTitle);
                        }*/
                        //HistoTable(aPeriods, tableTitle);
                        let bkaPeriods = JSON.parse(JSON.stringify(aPeriods));
                        genHTMLTable(aPeriods, "histoGraph")
                        D3HistoGraph(bkaPeriods);
                    } else if (params.type == "chart1Month") {
                        let sTypeChart = $('#lboChart').find(":selected").val();
                        if (sTypeChart == "BarChart") {
                            barChart(aPeriods, regionName);
                        } else if  (sTypeChart == "PieChart") {
                            pieChart(aPeriods, regionName);
                        }
                    } else if (params.type == "map1Month") {
                        fArchiveTable(aPeriods, regionName);
                    }
                    stopBusy();
                });

                var sCustomEvent = "chartDone" + (CDM_SG.bFirstMap ? "1" : "2");
                $("#panelGraph").trigger(sCustomEvent);
                CDM_SG.bFirstMap = false;
            }  // computeHistogram

        }

    }
);
