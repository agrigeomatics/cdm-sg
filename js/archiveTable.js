define([
        "esri/tasks/query",
        "esri/tasks/QueryTask",
        "CDM_SG/js/utilGraphs",
        "CDM_SG/js/ccs_raster"        
       ],
       function(Query, QueryTask, UtilGraphs, ccs_raster)
    {

        return {
            prepArchiveTable: function(rasterName) {
                var region = byId("lboRegion").options[byId("lboRegion").selectedIndex].value;
                var graphId = getGraphId();
                if (graphId.indexOf("Ext") >= 0 && graphId.indexOf("Area") >= 0) {
                    var results = {features: [{geometry: CDM_SG.selectedAgricExtent.geometry,
                                               attributes: {REGION_ID: 1,
                                                            region: region,
                                                            area: CDM_SG.selectedAgricExtent.area}
                                             }] };
                    UtilGraphs.computeHistogram({type: "map1Month"}, results);
                } else {
                    if (graphId.indexOf("Area") == -1) {
                        ccs_raster.calcVariable(graphId, "map1Month");
                    } else {
                        var query = new Query();
                        query.returnGeometry = true;
                        query.outFields = ["*"];
                        query.where = "REGION_ID = " + region;
                        var queryTask = new QueryTask(CDM_SG.urls.regions);
                        queryTask.execute(query, dojo.hitch(this, UtilGraphs.computeHistogram, {type: "map1Month"}));  
                        //queryTask.execute(query, UtilGraphs.computeHistogram);  
                    }
                }
            }
            
        }
    }    
);

function fArchiveTable(data) {
    console.log("fArchiveTable:", data);
    let titles = ["Selection", "Previous Month", "3 Months Prior", "Start of Growing Season", "One Year Prior"];
    for (var i=0; i < data.length; i++) {
        data[i].title = titles[i];
    }
    genHTMLTable(data, "archiveTable");
};

function genHTMLTable(data, graphType) {
    function getStyle(name) {
        var rgb = '';
        if (name == 'D0') rgb = '#FFFF00'; // '255,255,0';
        if (name == 'D1') rgb = '#FFD37F'; // '255,211,127';
        if (name == 'D2') rgb = '#E69800'; // '230,152,0';
        if (name == 'D3') rgb = '#E60000'; // '230,0,0';
        if (name == 'D4') rgb = '#730000'; // '115,0,0';
        if (name == 'noData') rgb = '#E1E1E1'; // '225,225,225';

        var color = name <= 'D2' || name == 'noData' ? 'black' : 'white';
        return ' style="background-color: ' + rgb + '; color: ' + color + ';"';
    }
    function fPct(num) {
        return dojo.number.format(num * 100, {places: 2}) + '%';
    }
    function fNum(num) {
        //if (graphId.indexOf("Area") >= 0)
        //    num /= 25000000;
        return dojo.number.format(num, {places: 0});
    }
    function getTh(num, graphId) {
        if (num == -1)
            return 'None';
        if (graphId.indexOf("Cum") >= 0 )
            return 'D' + num  + '-D4';
        else
            return 'D' + num;
    }
    
    data.sort(function(a, b) {
        if (a.date > b.date) return -1;
        if (a.date < b.date) return 1;
        return 0;
    });

    var graphId = getGraphId();

    let units;
    if ($( "#tabsMain").tabs("option", "active") == 1)
        units = "5 km2";
    else {
        let sVar = $('#lboVariable').find(":selected").val();
        let sText = $('#lboVariable').find(":selected").text();
        units = sVar;
        if (sVar == "Area") units = "5 km2";
        else  if (sVar == "PopTotal") units = "Inhabitants";
        else  if (sVar == "Pasture" || sVar == "FieldCrops" || sVar == "ForageCrops") units = "Hectares";
        else units = sText;
    }
    if (graphId.indexOf("Pct") > 0)
        byId("table-title-units").innerHTML = "Unit: Percentage";
    else 
        byId("table-title-units").innerHTML = "Unit: Number of " + units;

    for (var i=0; i < data.length; i++) {
        var d = data[i];
        var totalData = CDM_SG.graphInfo.totalData;    
        if (graphId.indexOf("Area") >= 0)
            totalData /= 25000000; //m2 to 5km2;

        var totPixels = d.D0 + d.D1 + d.D2 + d.D3 + d.D4 + d.noData;
        var ratio = totPixels / totalData;

        var totData = 0;
        for (var j = 0; j <= 4; j++) {
            d['D'+j] = d['D'+j] * ratio;
            totData += d['D'+j];
        };
        d.noData = d.noData * ratio;
        totData += d.noData; 

        d.totData = totData;
        d.fNoData = d.noData;
        d.fD4 = d.D4;
        if (graphId.indexOf("Cum") >= 0) {
            d.fD0 = d.D0 + d.D1 + d.D2 + d.D3 + d.D4;
            d.fD1 = d.D1 + d.D2 + d.D3 + d.D4;
            d.fD2 = d.D2 + d.D3 + d.D4;
            d.fD3 = d.D3 + d.D4;
        } else { // Categ
            d.fD0 = d.D0;
            d.fD1 = d.D1;
            d.fD2 = d.D2;
            d.fD3 = d.D3;
        }
        if (graphId.indexOf("Pct") >= 0) {
            d.fNoData /= totData;
            d.fD0 /= totData;
            d.fD1 /= totData;
            d.fD2 /= totData;
            d.fD3 /= totData;
            d.fD4 /= totData;
        }
    }

    var sHTML = '<table class="wb-tables table" graphtype ="'+ graphType + '">\n';
    sHTML +=  ' <thead>\n';
    sHTML +=  '  <tr style="background-color: #E1E1E1">\n';
    if (graphType == "archiveTable")
        sHTML +=  '   <th>Month</th>\n';
    sHTML +=  '   <th>Date</th>\n';
    sHTML +=  '   <th' + getStyle('noData') + '>' + getTh(-1, graphId) + '</th>\n';
    sHTML +=  '   <th' + getStyle('D0') + '>' + getTh(0, graphId) + '</th>\n';
    sHTML +=  '   <th' + getStyle('D1') + '>' + getTh(1, graphId) + '</th>\n';
    sHTML +=  '   <th' + getStyle('D2') + '>' + getTh(2, graphId) + '</th>\n';
    sHTML +=  '   <th' + getStyle('D3') + '>' + getTh(3, graphId) + '</th>\n';
    sHTML +=  '   <th' + getStyle('D4') + '>D4</th>\n';
    sHTML +=  '  </tr>\n';
    sHTML +=  ' </thead>\n';
    sHTML +=  ' <tbody>\n';
    for (var i = 0; i < data.length; i++) {
        sHTML +=  '  <tr>\n';
        if (graphType == "archiveTable")
            sHTML +=  '   <td>' + data[i].title + '</td>\n';
        sHTML +=  '   <td>' + data[i].date + '</td>\n';
        if (graphId.indexOf("Pct") >= 0) {
            sHTML +=  '   <td>' + fPct(data[i].fNoData) + '</td>\n';
            sHTML +=  '   <td>' + fPct(data[i].fD0) + '</td>\n';
            sHTML +=  '   <td>' + fPct(data[i].fD1) + '</td>\n';
            sHTML +=  '   <td>' + fPct(data[i].fD2) + '</td>\n';
            sHTML +=  '   <td>' + fPct(data[i].fD3) + '</td>\n';
            sHTML +=  '   <td>' + fPct(data[i].fD4) + '</td>\n';
            sHTML +=  '  </tr>\n';
        } else { // Categ
            sHTML +=  '   <td>' + fNum(data[i].fNoData) + '</td>\n';
            sHTML +=  '   <td>' + fNum(data[i].fD0) + '</td>\n';
            sHTML +=  '   <td>' + fNum(data[i].fD1) + '</td>\n';
            sHTML +=  '   <td>' + fNum(data[i].fD2) + '</td>\n';
            sHTML +=  '   <td>' + fNum(data[i].fD3) + '</td>\n';
            sHTML +=  '   <td>' + fNum(data[i].fD4) + '</td>\n';
            sHTML +=  '  </tr>\n';
        }
    }
    sHTML +=  ' </tbody>';
    sHTML +=  '</table>';

    var sTableResults = (graphType == "archiveTable" ? "MO" : "TS") + "divTableResults";
    byId(sTableResults).innerHTML = sHTML;

    $("#" + sTableResults + " tbody tr").each(function() {  
        this.onclick = function() {
            $("#" + sTableResults + " tbody tr").each(function() {  
                this.style.backgroundColor = "white";
            });
            this.style.backgroundColor='#BCD4EC';

            var graphType = $("#" + sTableResults + " table")[0].getAttribute("graphtype");
            var i = graphType == "histoGraph" ? 0 : 1;

            //if (graphType == "histoGraph") {
                var rasterName = this.cells[i].innerHTML;
                rasterName = rasterName.replace(/-/g, '_');
                rasterName = 'cdm_' + rasterName;
                require(["CDM_SG/js/mapManager"], function(mapManager) {
                    mapManager.showCDMLayer(rasterName);
                });
            //}
        };
    });
        
}
