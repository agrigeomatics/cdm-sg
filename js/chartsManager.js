/*
TO DO
*/

define([
        "esri/tasks/StatisticDefinition",       
        "esri/tasks/query",
        "esri/tasks/QueryTask",    
        "esri/geometry/geometryEngine",        
        "CDM_SG/js/utilGraphs",
        "CDM_SG/js/ccs_raster"
       ],
       function(StatisticDefinition,
                Query,
                QueryTask,
                geometryEngine,
                UtilGraphs,
                ccs_raster
            )
                 
    {
        
        return {

            execQuery: function () {
                var graphId = getGraphId();
                console.log("graphId: ", graphId);
                var formatId = $('#lboChart').find(":selected").val();
                
                var url2;
                var outField;
                var graphTitle;

                byId("MOdivGraphResults").innerHTML = "";

                if (graphId.indexOf("Area") == -1) {
                    ccs_raster.calcVariable(graphId, "chart1Month");
                    return;
                }

                var region = Number(byId("lboRegion").options[byId("lboRegion").selectedIndex].value);

                var statisticDefinition = new StatisticDefinition();
                statisticDefinition.statisticType = "sum";

                /*if (graphId.indexOf("Farms") >= 0 || graphId.indexOf("Cattle") >= 0) { //  || graphId.indexOf("PopTotal") >= 0
                    var query = new Query();
                    if (graphId.indexOf("Farms") >= 0) {
                        url2 = CDM_SG.urls.census_CCS + "7"; // 7 =  Land use and environmental practices (2016)
                        statisticDefinition.onStatisticField = "TOT_AREA_FARM_NUM";
                        outField = "FARM_NUM";
                        statisticDefinition.outStatisticFieldName = outField;
                        graphTitle = "Number of Farms by Drought Category";
                    }
                    if (graphId.indexOf("Cattle") >= 0) {
                        url2 = CDM_SG.urls.census_CCS + "8"; // 8 = Livestock (2016)
                        statisticDefinition.onStatisticField = "CATTLE_NUM";
                        outField = "CATTLE_NUM";
                        statisticDefinition.outStatisticFieldName = outField;
                        graphTitle = "Number of Cattle by Drought Category";
                    }
                
                    if (graphId.indexOf("PopTotal") >= 0) {
                        url2 = CDM_SG.urls.CCS_StatCan + "0"; // 0 = Census_Subdivision_Population (2016)
                        statisticDefinition.onStatisticField = "Population_2016";
                        outField = "Population_2016";
                        statisticDefinition.outStatisticFieldName = outField;
                        graphTitle = "Total Population by Drought Category";
                    }
                
                    query.groupByFieldsForStatistics = ["PRUID"];
                    query.returnGeometry = false;
                    query.outFields = ["PRUID", outField];
                    query.outStatistics = [statisticDefinition];
                    
                    if (region == 0) query.where = "PRUID in ('10','11','12','13')";
                    if (region == 1) query.where = "PRUID in ('24','35')";
                    if (region == 5) query.where = "PRUID in ('46','47','48')";
                    if (region == 4) query.where = "PRUID in ('59')";
                    if (region == 3) query.where = "PRUID in ('60','61','62')";
                    if (region == 6) query.where = "1=1";
                }*/

                if (graphId.indexOf("Area") >= 0) {
                    var query = new Query();
                    url2 = CDM_SG.urls.regions;
                    outField = "Shape_Area";
                    graphTitle = "Drought Category by Area";

                    query.returnGeometry = false;
                    query.outFields = ["region", "REGION_ID", outField];
                    query.where = "REGION_ID = " + region;
                }

                if (graphId.indexOf("Ext") >= 0) {
                    var query = new Query();
                    query.returnGeometry = true;
                    query.where = "OBJECTID >= 0";
                    query.maxAllowableOffset = 10000; // 1000
                    var queryTask = new QueryTask(CDM_SG.urls.agricExtent);
                    queryTask.execute(query, function(results) {
                        if (results.features.length != 1) {
                            alert(results.features.length + " polygon(s) in 'agrimap_canada_agricultural_extent_slc'!");
                            return;
                        }
    
                        var geometry = results.features[0].geometry;
                        //displayNbVertices(geometry, "Canada");
    
                        var query = new Query();
                        query.returnGeometry = true;
                        var region = $('#lboRegion').find(":selected");
                        query.where = "REGION_ID = " + Number(region.val());
                        var queryTask = new QueryTask(CDM_SG.urls.regions);
                        queryTask.execute(query, function(results) {
                            var intersect = geometryEngine.intersect(geometry, results.features[0].geometry);
                            require(["CDM_SG/js/mapManager"], function(mapManager) {
                                mapManager.addGraphic(intersect, "AgricExtent");
                            });
                            var intersectArea = geometryEngine.geodesicArea(intersect);
                            //displayNbVertices(intersect, region.text());
                            CDM_SG.selectedAgricExtent = {geometry: intersect, area: intersectArea};
                    
                            outField = "area";
                            graphTitle = "Drought Category by Agriculture Extent";
                            CDM_SG.graphInfo = {graphId: graphId, formatId: formatId, region: region, outField: outField,
                                                graphTitle: graphTitle};
                            CDM_SG.graphInfo.totalArea = intersectArea;

                            var results = {features: [{geometry: intersect,
                                                       attributes: {OBJECTID: 1,
                                                                     region: region.text(),
                                                                     area: intersectArea}
                                                     }] };
                            UtilGraphs.computeHistogram({type: "chart1Month"}, results);
                        });
                    });
                }

                if (graphId.indexOf("Ext") == -1) {
                    CDM_SG.graphInfo = {graphId: graphId, formatId: formatId, region: region, outField: outField,
                                        graphTitle: graphTitle};
                    var queryTask = new QueryTask(url2);
                    queryTask.execute(query, computeStats);
                }
            }
        }

        /*function displayNbVertices(geometry, sRegion) {
            var nbVertices = 0;
            for (var i = 0; i < geometry.rings.length; i++) {
                nbVertices += geometry.rings[i].length;
            }
            console.log("selectedAgricExtent " + sRegion + " nbVertices:", nbVertices); 
        }*/

        function computeStats(results) { // Laurier
            var totalData = 0;
            for (var i = 0; i < results.features.length; i++) {
                var totalData = totalData + results.features[i].attributes[CDM_SG.graphInfo.outField];
            }
            console.log("Total Data: ", CDM_SG.graphInfo.outField + " = " + totalData);
            CDM_SG.graphInfo.totalData = totalData;

            var query = new Query();
            query.returnGeometry = true;
            query.outFields = ["*"];
            query.where = "REGION_ID = " + CDM_SG.graphInfo.region;

            var url = CDM_SG.urls.regions;
            var queryTask = new QueryTask(url);
            queryTask.execute(query, dojo.hitch(this, UtilGraphs.computeHistogram, {type: "chart1Month"}));  
        }

    }
);

function prepareChart(dta, graphId) {
    var d = dta[0];
    var totalData = CDM_SG.graphInfo.totalData;    
    if (graphId.indexOf("Area") >= 0 && graphId.indexOf("Ext") >= 0)
        totalData /= 25000000; //m2 to 5km2;


    CDM_SG.graphInfo.yScaleTitle = $("#lboVariable option:selected").text();
    if (graphId.indexOf("Pct") > 0)
        CDM_SG.graphInfo.yScaleTitle += " (Percentage)";
    else {
        let units = null;
        let sVar = $('#lboVariable').find(":selected").val();
        let sText = $('#lboVariable').find(":selected").text();
        if (sVar == "Area") units = "5 km2";
        if (sVar == "Pasture" || sVar == "FieldCrops" || sVar == "ForageCrops") units = "Hectares";
        if (units != null)
            CDM_SG.graphInfo.yScaleTitle += " (" + units + ")";
    }
    
    var totPixels = d.D0 + d.D1 + d.D2 + d.D3 + d.D4 + d.noData;
    var ratio = totPixels / totalData;
    /*var data = [
        {code: "None", value: Math.round(d.noData * ratio)},
        {code: "D0", value: Math.round(d.D0 * ratio)},
        {code: "D1", value: Math.round(d.D1 * ratio)},
        {code: "D2", value: Math.round(d.D2 * ratio)},
        {code: "D3", value: Math.round(d.D3 * ratio)},
        {code: "D4", value: Math.round(d.D4 * ratio)}
    ];*/
    if (graphId.indexOf("Cum") > 0) {
        var data = [
            {code: "None", value: d.noData},
            {code: "D0", value: d.D0+d.D1+d.D2+d.D3+d.D4},
            {code: "D1", value: d.D1+d.D2+d.D3+d.D4},
            {code: "D2", value: d.D2+d.D3+d.D4},
            {code: "D3", value: d.D3+d.D4},
            {code: "D4", value: d.D4}
        ];
    } else {
        var data = [
            {code: "None", value: d.noData},
            {code: "D0", value: d.D0},
            {code: "D1", value: d.D1},
            {code: "D2", value: d.D2},
            {code: "D3", value: d.D3},
            {code: "D4", value: d.D4}
        ];
    }

    if (graphId.indexOf("Pct") > 0) {
        for (var i = 0; i < data.length; i++) {
            data[i].value = data[i].value / totPixels;
        }
    }
            
    console.log("prepareChart:", data);
    return data;
}

function barChart(dta, regionName) {
    var graphId = getGraphId();
    var formatPct = d3.format("%");
    var data = prepareChart(dta, graphId);
    var pctTitles = ["None", "D0-D4", "D1-D4", "D2-D4", "D4-D4", "D4"];

    var width0 = parseInt(getComputedStyle(byId("MOdivGraphResults")).width);
    var marginLeft = graphId.indexOf("Pct") >= 0 ? 40 : 70;
    var margin = {top: 20, right: 20, bottom: 50, left: marginLeft},
        padding = 100; // space around the chart, not including label
        width = width0, // 960,
        height = 400;

    var svg0 = d3.select("#MOdivGraphResults").append("svg")
        .attr("width", width)
        .attr("height", height)
      .append("g")
        .attr("transform", "translate(20,20)");

    var svg = svg0.append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    width = width - margin.left - margin.right;
    height = height - margin.top - margin.bottom;
                        
    var x = d3.scale.ordinal().rangeRoundBands([0, width], .1);
    var y = d3.scale.linear().range([height, 0]);

    x.domain(data.map(function(d) { return d.code; }));
    y.domain([0, d3.max(data, function(d) { return d.value; })]);
    
    if (graphId.indexOf("Cnt") >= 0) {
        var xAxis = d3.svg.axis().scale(x).orient("bottom");
        var yAxis = d3.svg.axis().scale(y).orient("left").ticks(10);
    }
    if (graphId.indexOf("Pct") >= 0) {
        //var xAxis = d3.svg.axis().scale(x).orient("bottom").tickFormat((d,i)=>pctTitles[i]);
        var xAxis = d3.svg.axis().scale(x).orient("bottom").tickFormat(function(d,i) { return pctTitles[i] });
        var yAxis = d3.svg.axis().scale(y).orient("left").tickFormat(formatPct);
    }
    
    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)
      .selectAll("text")
        .style("text-anchor", "end")
        .attr("dx", graphId.indexOf("Pct") >= 0 ? "1.5em" : "0.5em")
        .attr("dy", "1em")

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis);

    svg.selectAll("bar")
        .data(data)
      .enter()
        .append("rect")
        .style("fill", function(d) { return getColor(d.code); }) // "steelblue"
        .style("stroke", function(d) { return "black"; })
        .style("stroke-width", function(d) { return "0.2"; })
        .attr("x", function(d) { return x(d.code); })
        .attr("width", x.rangeBand())
        .attr("y", function(d) { return y(d.value); })
        .attr("height", function(d) { return height - y(d.value); });

    svg.selectAll(".text")  		
        .data(data)
      .enter()
        .append("text")
        .attr("class","label")
        .attr("class","label")
        .style("font-size", "12px")
        .attr("x", (function(d) {return x(d.code) + 50; }  ))
        .attr("y", function(d) { return y(d.value); })
        .attr("dy", "-0.5em")
        .text(function(d) { 
            if (graphId.indexOf("Pct") >= 0)
                return dojo.number.format(d.value, {type: 'percent', places: 2});            
            else
                return dojo.number.format(d.value);
         });         

    var layerName = CDM_SG.graphInfo.CDMLayerName;
    layerName = layerName.replace("cdm_", "");
    layerName = ' (' + layerName.replace(/_/g, '-') + ')';

    svg0.append("text")
        .attr("text-anchor", "middle")
        //.attr("transform", "translate("+ (padding/2) +","+ (height/2) + ")rotate(-90)")
          // text is drawn off the screen top left, move down and out and rotate
        .attr("transform", "translate(0, "+ (height/2) + ")rotate(-90)") 
         // text is drawn off the screen top left, move down and out and rotate
        .text(CDM_SG.graphInfo.yScaleTitle);    
            
    svg0.append("text")
        .attr("x", (width / 2))             
        .attr("y", 0)
        .attr("text-anchor", "middle")  
        .style("font-size", "larger") 
        .style("font-weight", "bold") 
        .style("height", "30px") 
        .text(CDM_SG.graphInfo.graphTitle + ' - ' + regionName + layerName);

    function getColor(name) {
        var rgb = '';
        if (name == 'D0') rgb = '#FFFF00'; // '255,255,0';
        if (name == 'D1') rgb = '#FFD37F'; // '255,211,127';
        if (name == 'D2') rgb = '#E69800'; // '230,152,0';
        if (name == 'D3') rgb = '#E60000'; // '230,0,0';
        if (name == 'D4') rgb = '#730000'; // '115,0,0';
        //if (name == 'None') rgb = '#E1E1E1'; // '225,225,225';
        if (name == 'None') rgb = 'white';
        return rgb;
    }
        
};
    
function pieChart(dta, regionName) {
    var graphId = getGraphId();
    var formatPct = d3.format("%");
    var data = prepareChart(dta, graphId);

    var width0 = parseInt(getComputedStyle(byId("MOdivGraphResults")).width);
    var margin = {top: 20, right: 20, bottom: 20, left: 20},
        width = width0, // 960, // - margin.left - margin.right
        height = 400; //  - margin.top - margin.bottom

    var radius = (height - 40) / 2;
    var aColors = [
        'rgb(255,255,255)', // 'noData'
        'rgb(255,255,0)',
        'rgb(255,211,127)',
        'rgb(230,152,0)',
        'rgb(230,0,0)',
        'rgb(115,0,0)'
    ];
   
    
    var svg0 = d3.select("#MOdivGraphResults")
      .append("svg")
        .data([data])
        .attr("width", width)
        .attr("height", height)
        .append("g")
        .attr("transform", "translate(20,20)");

        
    var svg = svg0.append("g")
        .attr("transform", 
              "translate(" + radius + "," + radius + ")");

    var pie = d3.layout.pie().value(function(d){
        return d.value;
    });
    
    // Declare an arc generator function
    //var arc = d3.svg.arc().outerRadius(radius);
    var arc = d3.svg.arc().outerRadius(radius)
                    .startAngle(function(d) { return d.startAngle + Math.PI/2; })
                    .endAngle(function(d) { return d.endAngle + Math.PI/2; });


    // Select paths, use arc generator to draw
    var arcs = svg.selectAll("g.slice")
            .data(pie)
            .enter()
            .append("g")
            .attr("class", "slice")
    ;
    arcs.append("path")
            .attr("fill", function(d, i){return aColors[i];})
            .attr("d", function (d) {return arc(d);})
            .style('stroke', 'silver')
            .style('stroke-width', 0.5);
    ;

    function calcInnerRadius(d) {
        for (var i=0; i<data.length; i++) {
            if (data[i].code == d.data.code)
              return 40 + i * 20;
        }
    }


    // Add the text
    arcs.append("text")
        .attr("transform", function(d){
            d.innerRadius = 100; /* Distance of label to the center*/
            //d.innerRadius = calcInnerRadius(d);
            d.outerRadius = radius;
            return "translate(" + arc.centroid(d) + ")";}
        )
        .attr("text-anchor", "middle")
        .text( function(d, i) {
            var retVal;
            if (graphId.indexOf("Pct") > 0)
                retVal =  dojo.number.format(data[i].value, {type: 'percent', places: 2});
            else                
                retVal =  dojo.number.format(data[i].value);
            return data[i].code + ': ' + retVal;
        })
    ;

    var layerName = CDM_SG.graphInfo.CDMLayerName;
    layerName = layerName.replace("cdm_", "");
    layerName = ' (' + layerName.replace(/_/g, '-') + ')';
    
    // Add the title
    svg0.append("text")
        .attr("x", (width / 2))             
        .attr("y", 0)
        .attr("text-anchor", "middle")  
        .style("font-size", "16px") 
        .text(CDM_SG.graphInfo.graphTitle + ' - ' + regionName + layerName + ' (Categorical)');
};

