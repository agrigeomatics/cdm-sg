define([
        "esri/tasks/query",
        "esri/tasks/QueryTask"
       ],
       function(
                Query,
                QueryTask
               )

    {
        return {

            initCCS_Variables: function() {

                // 1. Population
                var query = new Query();
                query.returnGeometry = false;
                query.where = "1=1";
                query.outFields = ["*"];
                var queryTask = new QueryTask(CDM_SG.urls.CCS_StatCan + '2'); // 2 = CCS_population
                queryTask.execute(query, function(results) {
                    var oCCSs = {};
                    for (var i = 0; i < results.features.length; i++) {
                        var attrs = results.features[i].attributes;
                        oCCSs[attrs.CCSUID] = {
                            FREQUENCY: attrs.FREQUENCY,
                            SUM_Population_2016: attrs.SUM_Population_2016,
                            DM: {},
                            nbDM: 0,
                            computedVariable: 0
                        };
                    }
                    CDM_SG.CCS = oCCSs;
    
                    // 2. Other Variables
                    var qryFarms = {
                        name: "Farms",
                        url: CDM_SG.urls.census_CCS + "7", // 7 = Land use and environmental practices (2016)
                        queryField: "OBJECTID",
                        outFields: ["OBJECTID", "CCSUID", "TOT_AREA_FARM_NUM"],
                        inFldName: "TOT_AREA_FARM_NUM",
                        outFldName: "nbFarms",
                        done: false,
                        iter: 1,
                        loops: null
                    }
                    queryCount(qryFarms);

                    qryCattle = {
                        name: "Cattle",
                        url: CDM_SG.urls.census_CCS + "8", // 8 = Livestock (2016)
                        queryField: "OBJECTID",
                        outFields: ["OBJECTID", "CCSUID", "CATTLE_NUM"],
                        inFldName: "CATTLE_NUM",
                        outFldName: "nbCattle",
                        done: false,
                        iter: 1,
                        loops: null
                    }
                    queryCount(qryCattle);
                            
                    qryPasture = {
                        name: "Pasture",
                        url: CDM_SG.urls.CANSIM + "1",
                        queryField: "OBJECTID",
                        outFields: ["OBJECTID", "CCSUID", "value2"],
                        inFldName: "value2",
                        outFldName: "nbPasture",
                        done: false,
                        iter: 1,
                        loops: null
                    }
                    queryCount(qryPasture);
                            
                    qryFieldCrops = {
                        name: "Field Crops",
                        url: CDM_SG.urls.CANSIM + "2",
                        queryField: "OBJECTID",
                        outFields: ["OBJECTID", "CCSUID", "value2"],
                        inFldName: "value2",
                        outFldName: "nbFieldCrops",
                        done: false,
                        iter: 1,
                        loops: null
                    }
                    queryCount(qryFieldCrops);
                            
                    qryForageCrops = {
                        name: "Forage Crops",
                        url: CDM_SG.urls.CANSIM + "3",
                        queryField: "OBJECTID",
                        outFields: ["OBJECTID", "CCSUID", "value2"],
                        inFldName: "value2",
                        outFldName: "nbForageCrops",
                        done: false,
                        iter: 1,
                        loops: null
                    }
                    queryCount(qryForageCrops);
                            
                });

            }
        }


        function init_Pasture() {
            var query = new Query();
            query.returnGeometry = false;
            query.where = "1=1";
            query.outFields = ["*"];
            //var queryTask = new QueryTask(CDM_SG.urls.census_CCS + "8"); // 8 = Livestock (2016)
            queryTask.execute(query, function(results) {
                for (var i = 0; i < results.features.length; i++) {
                    var attrs = results.features[i].attributes;
                    //var nbPasture = attrs.CATTLE_NUM;
                    //if (nbPasture == null) nbPasture = 0;
                    //CDM_SG.CCS[attrs.CCSUID].nbPasture = nbPasture;
                }
            });
        };


        //---------------
           
        function queryCount(oQry) {
            var queryTask = new QueryTask(oQry.url);
            var query = new Query();
            query.where = "1=1";
            query.outFields = oQry.outFields;
            queryTask.executeForCount(query,
                function(count) {
                    oQry.loops = Math.ceil(count/1000);
                    var sWhere = oQry.queryField + " > 0";
                    doQueryValues(oQry, sWhere);
                },
                function(error){
                    console.log(error);
                }
            );
        };

        function doQueryValues(oQry, sWhere) {
            
            function queryTaskResults(results) {
                console.log(oQry.name, oQry.iter, oQry.loops, results.features.length)
                for (var i = 0; i < results.features.length; i++) {
                    var attrs = results.features[i].attributes;
                    var value = attrs[oQry.inFldName];
                    if (value == null) value = 0;
                    //try {
                        CDM_SG.CCS[attrs.CCSUID][oQry.outFldName] = value;
                    /*} catch(e) {
                        console.log(Object.getOwnPropertyNames(CDM_SG.CCS).length);
                        debugger;
                    }*/
                };
                if (oQry.iter < oQry.loops) {
                    oQry.iter += 1;
                    var lastOID = results.features[i-1].attributes[oQry.queryField];
                    var sWhere = oQry.queryField + " > " + lastOID;
                    doQueryValues(oQry, sWhere);
                } else
                    oQry.done = true;
            };        
    
            var queryTask = new QueryTask(oQry.url);
            var query = new Query();
            query.where = sWhere;
            query.returnGeometry = false;
            query.outFields = oQry.outFields;
            query.orderByFields = [oQry.queryField];
            queryTask.execute(query, queryTaskResults);
        };
    
    }
);
