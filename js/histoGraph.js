define([
        "esri/tasks/query",
        "esri/tasks/QueryTask",    
        "CDM_SG/js/utilGraphs"
       ],
       function(Query,
                QueryTask,
                UtilGraphs)
                
    {
    
    var url =  CDM_SG.urls.regions;

    return {

        execQuery: function () {

            var startPeriod = $('#lboStartYear').find(":selected").val() +
                             $('#lboStartMonth').find(":selected").val();
            var endPeriod = $('#lboEndYear').find(":selected").val() +
                           $('#lboEndMonth').find(":selected").val();
            if (startPeriod >= endPeriod) {
                alert("You must define a time interval.");
                return;
            }

            byId("TSdivGraphResults").innerHTML = "";
            
            var region = byId("lboRegion").options[byId("lboRegion").selectedIndex].value;
            var query = new Query();
            query.returnGeometry = true;
            query.outFields = ["*"];
            query.where = "REGION_ID = " + region;
            var queryTask = new QueryTask(url);
            //queryTask.execute(query, UtilGraphs.computeHistogram, true);
            queryTask.execute(query, dojo.hitch(this, UtilGraphs.computeHistogram, {type: "histoGraph"}));
        }
    }

});

function prepareHistoGraph(data, graphId) {
    for (var i = 0; i < data.length; i++) {
        delete data[i].noData;
    }
    console.log("prepareHistoGraph:", data);
    return data;
}

function D3HistoGraph(dta) {
    var graphId = getGraphId();
    var formatPct = d3.format("%");
    var data = JSON.parse(JSON.stringify(dta));
    data = prepareHistoGraph(data, graphId);

    var width0 = parseInt(getComputedStyle(byId("TSdivGraphResults")).width);  //  960
    var height0 = 400;
    var margin = {top: 20, right: 20, bottom: 50, left: 60},
        width = width0 - margin.left - margin.right,
        height = height0 - margin.top - margin.bottom;

    var parseDate = d3.time.format("%y-%b-%d").parse;
    //var parseDate = d3.time.format("%Y-%m-%d").parse;

    var x = d3.time.scale()
        .range([0, width]);

    var y = d3.scale.linear()
        .range([height, 0]);

    var color = d3.scale.category20();

    var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom");

    /*var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left")
        //.tickFormat(formatPercent);*/

    //if (graphId.indexOf("Cnt") >= 0)
        var yAxis = d3.svg.axis().scale(y).orient("left").ticks(10);
    //if (graphId.indexOf("Pct") >= 0)
    //    var yAxis = d3.svg.axis().scale(y).orient("left").tickFormat(formatPct);


    // An area generator.
    /*var area = d3.svg.area()
        .x(function(d) { return x(d.date); })
        .y0(function(d) { return y(d.y0); })
        .y1(function(d) { return y(d.y0 + d.y); });*/
    var area = d3.svg.area()
        .x(function(d) {
            //if (d.D4) debugger;
            var ret = x(d.date);
            //console.log( " x:", d, ret);
            return ret; })
        .y0(function(d) { 
            //if (d.D4) return null;
            var ret = y(d.y0);
            //console.log( " y0:", d, ret);
            return ret; })
        .y1(function(d) {
            //if (d.D4) return null;
            var ret = y(d.y0 + d.y);
            //console.log( " y1:", d, ret);
            return ret;
        });

    var stack = d3.layout.stack()
        .values(function(d) { return d.values; });


    var svg0 = d3.select("#TSdivGraphResults").append("svg")
        .attr("width", width0)
        .attr("height", height0)
      .append("g")
        .attr("transform", "translate(20,20)");
        
    var svg = svg0.append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    color.domain(d3.keys(data[0]).filter(function(key) { return key !== "date"; }));
    data.forEach(function(d) {
        //d.date = parseDate(d.date);
        d.date = new Date(d.date + 'T12:00:00');
    });

    var rect = svg.append("svg:rect")
        .attr("class", "pane")
        .attr("width", width)
        .attr("height", height);
        
    
    var browsers = stack(color.domain().map(function(name) {
        return {
            name: name,
            values: data.map(function(d) {
                return {date: d.date, y: d[name] * 1};
            })
        };
    }));

    // Find the value of the day with highest total value
    var maxDateVal = d3.max(data, function(d){
        var vals = d3.keys(d).map(function(key){ return key !== "date" ? d[key] : 0 });
        return d3.sum(vals);
    });

    // Set domains for axes
    x.domain(d3.extent(data, function(d) { return d.date; }));
    y.domain([0, maxDateVal]);
  
    rect.call(d3.behavior.zoom().x(x).on("zoom", zoom));  

    var browser = svg.selectAll(".browser")
        .data(browsers)
      .enter().append("g")
        .attr("class", "browser");

        browser.append("path")
            .attr("class", "area")
            .attr("d", function(d) {
              //console.log(d.name);
              return area(d.values); })
            .style("fill", function(d) {
               var x = getColor(d.name);
               return x
            });

        /*browser.append("text")
            .datum(function(d) { return {name: d.name, value: d.values[d.values.length - 1]}; })
            .attr("transform", function(d) {
                     return "translate(" + x(d.value.date) + "," + y(d.value.y0 + d.value.y / 2) + ")";
                   })
            .attr("x", -6)
            .attr("dy", ".35em")
            .text(function(d) { return d.name; });*/

    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis);
            
    svg0.append("text")
        .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
        //.attr("transform", "translate("+ (padding/2) +","+ (height/2) + ")rotate(-90)")  // text is drawn off the screen top left, move down and out and rotate
        .attr("transform", "translate(0, "+ (height/2) + ")rotate(-90)")  // text is drawn off the screen top left, move down and out and rotate
        .text(CDM_SG.graphInfo.yScaleTitle);    
        
    svg0.append("text")
        .attr("x", (width / 2))             
        .attr("y", 0)
        .attr("text-anchor", "middle")  
        .style("font-size", "larger") 
        .style("font-weight", "bold") 
        .text(CDM_SG.graphInfo.graphTitle); //  + ' - ' + regionName + layerName
        
    // Bind the data to our path elements.
    svg.select("path.area").data([data]);
    //svg.select("path.line").data([data]);

    rect.call(d3.behavior.zoom().x(x).on("zoom", zoom));          
      
    draw();      
      
    //-----

    function getColor(name) {
        var rgb = '';
        if (name == 'D0') rgb = '#FFFF00'; // '255,255,0';
        if (name == 'D1') rgb = '#FFD37F'; // '255,211,127';
        if (name == 'D2') rgb = '#E69800'; // '230,152,0';
        if (name == 'D3') rgb = '#E60000'; // '230,0,0';
        if (name == 'D4') rgb = '#730000'; // '115,0,0';
        if (name == 'noData') rgb = '#E1E1E1'; // '225,225,225';
        return rgb;
    }

    function draw() {
        svg.select("g.x.axis").call(xAxis);
        svg.select("g.y.axis").call(yAxis);
        svg.selectAll("path")
          .data(browsers)
            .attr("d", function(d) {
              //console.log(d.name);
              return area(d.values); })
            .style("fill", function(d) {
               var x = getColor(d.name);
               return x
            });
    }
      
    function zoom() {
        //d3.event.transform(x); // TODO d3.behavior.zoom should support extents
        draw();
    }
    
};
