function initApp() {
    $.getJSON("config/config.json", function(data) {
        window.CDM_SG = data.CDM_SG;
        CDM_SG.bFirstMap = true;
        require(["CDM_SG/js/mapManager", "CDM_SG/js/initVariables"],
                function(mapManager, initVariables) {
            mapManager.initMap();
            mapManager.showFirstCDMLayer();

            initVariables.initCCS_Variables();
        });
    });
    $("#tabsMain").tabs();
    byId('paneTable').appendChild(byId('MOdivTableResults'));
    byId('paneGraph').appendChild(byId('MOdivGraphResults'));
}

//--------------------------------------------------------------------------------------------------
// Common and Utilities functions
//--------------------------------------------------------------------------------------------------

function byId(id) {
    return document.getElementById(id)
}

function getGraphId() {
    let sArea = $('#lboArea').find(":selected").val();
    let sAggr = $('#lboAggregation').find(":selected").val();
    let sStat = $('#lboStatistic').find(":selected").val();
    let sVar  = $('#lboVariable').find(":selected").val();

    let graphId = sArea + sAggr + sStat + sVar;
    return graphId;
}

function getRasterName() {
    var sYear = $('#lboYear').find(":selected").val();
    var sMonth = $('#lboMonth').find(":selected").val();
    if (Number(sMonth) < 12) {
        var sMonth = (Number(sMonth) + 1).toString();
        sMonth = sMonth.length == 1 ? '0' + sMonth : sMonth;
        var sDate = sYear + '-' + sMonth + "-01T12:00:00";
    } else {
        var sDate = (Number(sYear) + 1) + "-01-01T12:00:00";
    }
    dDate = new Date(sDate);
    var dte1 = new Date(dDate.getTime());
    dte1 = new Date(dte1.setDate(dte1.getDate()-1));
    var rasterName = dte1.toISOString().substr(0,10);
    rasterName = rasterName.replace(/-/g, '_');
    rasterName = 'cdm_' + rasterName;
    console.log("rasterName: ", rasterName);
    return rasterName;
}
