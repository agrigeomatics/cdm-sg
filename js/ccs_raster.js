define(["esri/layers/RasterFunction",
        "esri/layers/RasterLayer", 
        "esri/layers/MosaicRule", 
        "esri/layers/ImageServiceParameters",
        "esri/tasks/query",
        "esri/tasks/QueryTask",
        "dojo/topic",
        "dojo/promise/all"
    ],
       function(RasterFunction,
                RasterLayer,
                MosaicRule,
                ImageServiceParameters,
                Query,
                QueryTask,
                topic,
                promiseAll)

    {
        var params = {};
        var aRasters = [];
        var aPeriods = [];
        var variableField = "";

        return {

            calcVariable: function(graphId,sType) {
                params = {};
                aRasters = [];
                aPeriods = [];
                byId("MOdivTableResults").innerHTML = "";

                params.type = sType;                
                console.log('\nStart:', new Date().toISOString());
                startBusy();
                var aPromises = [];
            
                var url =  CDM_SG.urls.CDM;

                var sYear = $('#lboYear').find(":selected").val();
                var sMonth = $('#lboMonth').find(":selected").val();
                if (Number(sMonth) < 12) {
                    var sMonth = (Number(sMonth) + 1).toString();
                    sMonth = sMonth.length == 1 ? '0' + sMonth : sMonth;
                    var sDate = sYear + '-' + sMonth + "-01T12:00:00";
                } else {
                    var sDate = (Number(sYear) + 1) + "-01-01T12:00:00";
                }
                var startDate = new Date(sDate);

                params.max = sType == "chart1Month" ? 0 : 4;
                let titles = ["Selection",
                              "Previous Month",
                              "3 Months Prior",
                              "Start of Growing Season",
                              "One Year Prior"];

                if (graphId.indexOf("PopTotal") >= 0) {
                    CDM_SG.graphInfo.graphTitle = "Total Population by Drought Category";
                    variableField = "SUM_Population_2016";
                } else if (graphId.indexOf("Farms") >= 0) {
                    CDM_SG.graphInfo.graphTitle = "Number of Farms by Drought Category";
                    variableField = "nbFarms";
                } else if (graphId.indexOf("Cattle") >= 0) {
                    CDM_SG.graphInfo.graphTitle = "Number of Heads of Cattle by Drought Category";
                    variableField = "nbCattle";
                } else if (graphId.indexOf("Pasture") >= 0) {
                    CDM_SG.graphInfo.graphTitle = "Pasture Hectares by Drought Category";
                    variableField = "nbPasture";
                } else if (graphId.indexOf("FieldCrops") >= 0) {
                    CDM_SG.graphInfo.graphTitle = "Field Crops Hectares by Drought Category";
                    variableField = "nbFieldCrops";
                } else if (graphId.indexOf("ForageCrops") >= 0) {
                    CDM_SG.graphInfo.graphTitle = "Forage Crops Hectares by Drought Category";
                    variableField = "nbForageCrops";
                }

                for (let i = 0; i <= params.max; i++) {
                    if (i==0) {
                        var rasterName = getRasterName();
                        aRasters[0] = {rasterName: rasterName, title: titles[i]};
                        //CDM_SG.graphInfo.CDMLayerName = rasterName;
                    } else {
                        var strtDte = new Date(startDate.getTime());
                        var dte = new Date(startDate.getTime());
                        switch(i) {
                            case 1:   // 1 months ago
                                dte = new Date(dte.setMonth(strtDte.getMonth()-1 ));
                                dte = new Date(dte.setDate(dte.getDate()-1 ));
                                break;
                            case 2:   // 3 months ago
                                dte = new Date(dte.setMonth(strtDte.getMonth()-3 ));
                                dte = new Date(dte.setDate(dte.getDate()-1 ));
                                break;
                            case 3:   // begin of growing season
                                var sDate = strtDte.getFullYear() + "-05-01T12:00:00";
                                dte = new Date(sDate);
                                dte = new Date(dte.setDate(dte.getDate()-1 ));
                                break;
                            case 4:  // 1 year ago
                                dte = new Date(dte.setYear(strtDte.getFullYear()-1 ));
                                dte = new Date(dte.setDate(dte.getDate()-1 ));
                                break;
                        }
                        var sdte1 = dte.toISOString().substr(0,10);
                        var sdte2 = sdte1.replace(/-/g, "_");
                        var rasterName = "cdm_" + sdte2;
                        aRasters[i] = {rasterName: rasterName, title: titles[i]};
                    }

                    var query = new Query();
                    query.returnGeometry = false;
                    query.outFields = ["*"];
                    query.where = "name = '" + rasterName + "'";
                    var queryTask = new QueryTask(url);
                    aPromises.push(queryTask.execute(query));
                }

                promiseAll(aPromises).then(function(aResults) {
                    for (let i = 0; i < aResults.length; i++) {
                        aRasters[i].OID = aResults[i].features[0].attributes.OBJECTID;
                        genRaster(aRasters[i].OID);
                    }
                })

            }
        }

        function genRaster(selectedCDM) {
            // Add 1 to selected CDM
            var CDMplus1 = new RasterFunction();
            CDMplus1.functionName = "Arithmetic";
            CDMplus1.functionArguments = {
                "Raster": "$" + selectedCDM,
                "Operation": 1,     // add
                "Raster2": 1
            };                 

            // Multiply CCCS values by 10
            var CCSby10 = new RasterFunction();
            CCSby10.functionName = "Arithmetic";
            CCSby10.functionArguments = {
                "Raster": "$184",   // fishhet25_raster
                "Operation": 3,     // multiply
                "Raster2": 10
            };                 

            // Add CDM values to CCS raster
            var CCSplusCDM = new RasterFunction();
            CCSplusCDM.functionName = "Arithmetic";
            CCSplusCDM.functionArguments = {
                "Raster": CCSby10,
                "Operation": 1,     // add
                "Raster2": CDMplus1,
                "ExtentType": 0,    // esriExtentFirstOf
                "CellsizeType": 2   // esriCellsizeMaxOf
            };

            var ISParams = new ImageServiceParameters();
            ISParams.renderingRule = CCSplusCDM;

            if (getGraphId().indexOf("Ext") >= 0) {
                var CCSplusCDM_Ext = new RasterFunction();
                CCSplusCDM_Ext.functionName = "Arithmetic";
                CCSplusCDM_Ext.functionArguments = {
                    "Raster": CCSplusCDM,
                    "Operation": 1,     // add
                    "Raster2": "$185",
                    "ExtentType": 0,    // esriExtentFirstOf
                    "CellsizeType": 2   // esriCellsizeMaxOf
                    };
                ISParams.renderingRule = CCSplusCDM_Ext;
            }

            var proxyUrl = "/DotNet/proxy.ashx";
            var rasterUrl = proxyUrl + "?" + CDM_SG.urls.CDM;
            var rasterUrl = CDM_SG.urls.CDM;
            
            //Define the raster layer
            var rasterLayer = new RasterLayer(rasterUrl, {
                id: "CCSplusCDM_" + selectedCDM,
                pixelFilter: maskPixels,
                imageServiceParameters: ISParams
            });
            CDM_SG.map.addLayer(rasterLayer, 1);
        }

        function maskPixels(pixelData) {
            if (pixelData == null || pixelData.pixelBlock == null)
                return;
            var pixelBlock = pixelData.pixelBlock;
            var pixels = pixelBlock.pixels[0];
            var invalidCCS = 0;
            var aInvalidCCS = {};
            var invalidDM = 0;
            var aInvalidDM = {};
            console.log("\ngenRaster, ", this.id);

            var aProvs = [];
            var region = $('#lboRegion').find(":selected").val();
            if (region == 1) aProvs = [10,11,12,13];
            if (region == 2) aProvs = [24,35];
            if (region == 5) aProvs = [46,47,48];
            if (region == 4) aProvs = [59];
            if (region == 3) aProvs = [60,61,62];
            if (region == 6) aProvs = [10,11,12,13,24,35,46,47,48,59,60,61,62];
            var aPopRegion = [null, 2333322, 8164361+13448494, 77660, 4648055, 6443892, 35115784];

            //pixels.sort(function(a, b) { return a-b });

            for (let CCS in CDM_SG.CCS) {
                CDM_SG.CCS[CCS].computedVariable = 0;
                CDM_SG.CCS[CCS].nbDM = 0;
                CDM_SG.CCS[CCS].DM = {};
            }

            let bkOID = null;
            let CSS = null;
            var cntOID0 = 0;
            let CCSwithNoData = {};
            var aMask = {};
            for (let i = 0; i < pixels.length ; i++) {
                if (i % 100000 == 0)
                    console.log(i);
                var pixel = pixels[i];
                var OID = Math.floor(pixel / 10);
                var DM = (pixel % 10) - 1;

                /*var jj = pixelBlock.mask[i];
                if (!aMask['mask'+jj]) 
                    aMask['mask'+jj] = 0;
                aMask['mask'+jj] += 1;
                if (jj == 0)
                    continue; // skip to next pixel*/

                if (typeof DM != "number" || !(DM >= -1 && DM <= 5)) {
                    invalidDM += 1;
                    if (CDM_SG.CCS_fishnet25[OID])
                        var CCS = getCCSUID(CDM_SG.CCS_fishnet25[OID][0]);
                    else
                        var CCS = 'null';
                    let key = CCS + '_' + DM;
                    if (aInvalidDM[key]) aInvalidDM[key] += 1; else aInvalidDM[key] = 1;
                } else {
                    if (OID > 0) {
                        var sDM = DM == -1 ? "noDrought" : DM == 5 ? "noData" : "D" + DM;

                        if (CDM_SG.CCS_fishnet25[OID]) {
                            if (sDM != "noData") {
                                for (let j = 0; j < CDM_SG.CCS_fishnet25[OID].length ; j++) {
                                    let CCS = getCCSUID(CDM_SG.CCS_fishnet25[OID][j]);
                                    if (!CDM_SG.CCS[CCS]) {
                                        invalidCCS += 1;
                                        let key = OID + '_' + CCS;
                                        if (aInvalidCCS[key]) aInvalidCCS[key] += 1; else aInvalidCCS[key] = 1;
                                    } else {
                                        var prov = Number(CCS.substr(0,2));
                                        if (aProvs.indexOf(prov) >= 0) {
                                            if (CDM_SG.CCS[CCS].DM[sDM])
                                                CDM_SG.CCS[CCS].DM[sDM] += 1;
                                            else {
                                                CDM_SG.CCS[CCS].DM[sDM] = 1;
                                                CDM_SG.CCS[CCS].nbDM += 1;
                                            }
                                        }
                                    }   
                                }
                            } else {
                                let CCS = getCCSUID(CDM_SG.CCS_fishnet25[OID]);
                                if (CCSwithNoData[CCS])
                                    CCSwithNoData[CCS] += 1;
                                else
                                    CCSwithNoData[CCS] = 1;
                            }
                        }
                    } else
                        cntOID0 += 1;
                }                           
            }

            console.log("aInvalidCCS: ", JSON.stringify(aInvalidCCS));
            console.log("aInvalidDM: ", JSON.stringify(aInvalidDM));
            console.log("CCSwithNoData: ", JSON.stringify(CCSwithNoData));

            CDM_SG.DM = {"noDrought": 0, "D0": 0, "D1": 0, "D2": 0, "D3": 0, "D4": 0, "noData": 0};
            for (let CCS in CDM_SG.CCS) {
                let prov = Number(CCS.substr(0,2));
                if (aProvs.indexOf(prov) >= 0) {
                    if (CDM_SG.CCS[CCS].nbDM > 1) {
                        var totPixels = 0;
                        var totVariable = CDM_SG.CCS[CCS][variableField];
                        for (var prop in CDM_SG.CCS[CCS].DM) {
                            totPixels += CDM_SG.CCS[CCS].DM[prop];
                        }
                        for (var prop in CDM_SG.CCS[CCS].DM) {
                            var ratio  = CDM_SG.CCS[CCS].DM[prop] / totPixels;
                            var variableCCS = CDM_SG.CCS[CCS][variableField] * ratio;
                            CDM_SG.DM[prop] += variableCCS;
                            CDM_SG.CCS[CCS].computedVariable += variableCCS;
                        }
                    }
                    if (CDM_SG.CCS[CCS].nbDM == 1) {
                        for (var prop in CDM_SG.CCS[CCS].DM) {
                            CDM_SG.DM[prop] += CDM_SG.CCS[CCS][variableField];
                            break;
                        }
                        CDM_SG.CCS[CCS].computedVariable += CDM_SG.CCS[CCS][variableField];
                    }
                    if (CDM_SG.CCS[CCS].nbDM == 0) {
                        console.log ("CCS", CCS, "has no DM!")
                    }
                }
            }

            let tot = 0;
            for (let prop in CDM_SG.DM) {
                if (prop != 'noData') {
                    CDM_SG.DM[prop] = Math.round(CDM_SG.DM[prop]);
                    tot += CDM_SG.DM[prop];
                }
            }

            console.log("CDM_SG.DM:", JSON.stringify(CDM_SG.DM));
            verifyVariable(aProvs);
            
            if (variableField == "PopTotal")
                console.log("Done: Pop: " + dojo.number.format(tot) +
                        "; Census 2016: " + dojo.number.format(aPopRegion[region]) +
                        "; DIFF: " + dojo.number.format(aPopRegion[region] - tot), '\n' );

            let sDate, sTitle;
            for (let i = 0; i < aRasters.length ; i++) {
                if ("CCSplusCDM_" + aRasters[i].OID == this.id) {
                    sDate = aRasters[i].rasterName;
                    sTitle = aRasters[i].title;
                    break;
                }
            }
            sDate = sDate.replace(/CDM_/i, '');
            sDate = sDate.replace(/_/g, '-');

            aPeriods.push({date: sDate, title: sTitle,
                           D0: CDM_SG.DM.D0, D1: CDM_SG.DM.D1,     D2: CDM_SG.DM.D2,
                           D3: CDM_SG.DM.D3, D4: CDM_SG.DM.D4, noData: CDM_SG.DM.noDrought});
            if (params.type == "chart1Month")
                CDM_SG.graphInfo.totalData = tot;

            let allDone = false;
            if (params.type == "chart1Month" && aPeriods.length == 1) allDone = true;
            if (params.type == "map1Month"   && aPeriods.length == 5) allDone = true;
                
            if (allDone) {
                var regionName = $('#lboRegion').find(":selected").text();
                if (params.type == "chart1Month") {
                    let sTypeChart = $('#lboChart').find(":selected").val();
                    if (sTypeChart == "BarChart") {
                        barChart(aPeriods, regionName);
                    } else if  (sTypeChart == "PieChart") {
                        pieChart(aPeriods, regionName);
                    }
                } else if (params.type == "map1Month") {
                    //fArchiveTable(aPeriods, regionName);
                    genHTMLTable(aPeriods, "archiveTable");
                }

                window.setTimeout(function() {
                    for (let i = CDM_SG.map.layerIds.length - 1; i>= 0; i--) {
                        var layerId = CDM_SG.map.layerIds[i];
                        if (layerId.indexOf("CCSplusCDM") >= 0) {
                            console.log("Removing layer", params.type, layerId);
                            CDM_SG.map.removeLayer(CDM_SG.map.getLayer(layerId));
                        }
                    }
                    console.log(CDM_SG.map.layerIds);
                    if (params.type == "chart1Month")
                        $("#panelGraph").trigger("chartDone2");
                    console.log("All Done: ", params.type,  new Date().toISOString());
                }, 1000);

                stopBusy();
            }
        }

        function getCCSUID(OID) {
            for (let i = 0; i < CDM_SG.aCCSuid.length; i++) {
                if (CDM_SG.aCCSuid[i].OBJECTID == OID)
                    return CDM_SG.aCCSuid[i].CCSUID;
            }
            return null;
        }
    
        function verifyVariable(aProvs) {
            console.log("Mismatch CCS variable data:");
            var totVar = 0;
            for (prop in CDM_SG.CCS) {
                var prov = Number(prop.substr(0,2));
                if (aProvs.indexOf(prov) >= 0) {
                    CDM_SG.CCS[prop].computedVariable = Math.round(CDM_SG.CCS[prop].computedVariable);
                    if (CDM_SG.CCS[prop][variableField] !=  CDM_SG.CCS[prop].computedVariable)
                        console.log(' ', prop, CDM_SG.CCS[prop][variableField],  CDM_SG.CCS[prop].computedVariable);
                        totVar += CDM_SG.CCS[prop].computedVariable;
                }
            }
            console.log("totVar:" , totVar);
        }
    
    }
)
